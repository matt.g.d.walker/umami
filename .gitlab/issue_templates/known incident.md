[[_TOC_]]
## Incident Report for a known / recuring situation
<!-- Use if the incident is a recurring issue, with an already validated change procedure -->
<!-- Add reference to already validated related incident or Emergency change with /relate -->
/relate nomadic-labs/umami-wallet/umami#xyz

### What is the nature of the INC?
<!-- Which parts of the system where affected ? -->


### How the INC was discovered?


### What is the potential impact of the INC?


### What is the evidence (i.e. screenshots, logs, etc)?


<!-- METADATA for project management, please leave the following lines and edit as needed -->
# Metadata
<!-- Severity : pick one the gitlab panel, right side of the window when viewing the incident after creation -->

/label ~INC ~incident  
<!-- Labels and default review status for gitlab Change management process, comment if no change was performed-->
/label ~Change ~"Change::Emergency"

## Reviews (none required)
<!-- No review required since this is a known incident with already validated change procedure -->

<!-- If you want to bring the attention to this incident during next CAB meeting, uncomment the next line -->
<!-- /label ~Change ~"CAB::to-review" -->

<!-- ( ping CAB members : @picdc @remyzorg @comeh @philippewang.info @SamREye ) -->
<!-- /unlabel ~"CAB::to-review" -->

<!-- METADATA - end -->
