[[_TOC_]]
## Incident Report

<!-- ### Checklist -->
<!-- Before filing an incident report, if there are any doubt, follow the checklist to ensure you are in fact dealing with an incident: !-->
<!--
 - [] 1. the situation impacts the live production environment
 - [] 2. the situation impacts the users access to mainnet chain services or assets
 - [] 3. the situation is severe or it carries a high risk; in that **any** of the following is true:
   -  [] it will/may lead to users being unable to view their assets 
   -  [] it will/may lead to users being unable to access their assets
   -  [] it will/may lead to downtime of the system
   -  [] it will/may lead to downtime to a feature of the system (which is not purely informational)
 - [] 4. the adverse impact is urgent; in that **all** of the following is true:
   - [] is ongoing or is imminent
   - [] no straightforward workaround is possible
   - [] solution cannot wait for the next planned release
-->
<!-- If you can tick boxes 1, 2, 3, and 4--you have uncovered an Incident; otherwise, it may not be, please consult management or you may file it anyway if unsure. !-->

### What is the nature of the INC?
<!-- Which parts of the system where affected ? -->


### How the INC was discovered?


### What is the potential impact of the INC?


### What is the evidence (i.e. screenshots, logs, etc)?


<!-- METADATA for project management, please leave the following lines and edit as needed -->
# Metadata
<!-- Severity : pick one the gitlab panel, right side of the window when viewing the incident after creation -->

/label ~INC ~incident  
<!-- Labels and default review status for gitlab Change management process, comment if no change was performed-->
/label ~Change ~"CAB::to-review" ~"Change::Emergency" 

## Reviews (all required)
( ping CAB members : @picdc @remyzorg @comeh @philippewang.info @SamREye )
- [ ] Incident Review from Development 
- [ ] Incident Review from Operations 
- [ ] Incident Review from Business 
<!-- check the box [x], you may also add your @user handle at the end of the line -->
<!-- Quick actions for last reviewer : -->
<!-- /unlabel ~"CAB::to-review" -->

<!-- METADATA - end -->
