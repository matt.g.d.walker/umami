[[_TOC_]]
## Emergency change
<!-- Emergency change should be filed whenever an incident caused a required change. It will be reviewed by the CAB afterward !-->

<!-- /confidential -->
<!-- If confidential, explain why -->

### Summary
<!-- Outline the issue being faced, and why this required a change !-->

### Area of the system
<!-- This might only be one part, but may involve multiple sections !-->

### How does this currently work?
<!-- The current process, and any associated business rules !-->

### What is the desired way of working?
<!-- After the change, what should the process be, and what should the business rules be !-->

<!-- Success criteria of change application (when relevant, include how to test) -->

### Change Procedure
- [ ] Change procedure been tested successfully

<!-- Include step by step description of changes performed -->


## Rollback plan
<!-- Describe how to rollback the change in case the expected change is not working -->


<!-- METADATA for project management, please leave the following lines and edit as needed -->
# Metadata
<!-- PRIORITY: Uncomment /label quick actions as appropriate. The priority and severity assigned may be different to this !-->
<!--High : (This will bring a huge increase in performance/productivity/usability, or is a legislative requirement)-->
<!-- /label ~"Priority::1-High" -->
<!--Medium : (This will bring a good increase in performance/productivity/usability)-->
<!-- /label ~"Priority::2-Medium" -->
<!--Low : (anything else e.g., trivial, minor improvements) -->
<!--  /label ~"Priority::3-Low" -->

# Reviews (all required)
( ping CAB members : @picdc @remyzorg @comeh @philippewang.info @SamREye )
- [ ] Review from Development
- [ ] Review from Operations
- [ ] Review from Business  
<!-- check the box [x], you may also add your @user handle at the end of the line -->
<!-- Quick actions for last reviewer : -->
<!-- /unlabel ~"CAB::to-review" -->

/label ~"Change::Emergency"  <!-- Emergency change request, usually on incident, sent to the CAB for review after applying. -->
/label ~Change ~"CAB::to-review" <!-- labels for gitlab CAB Change issues management -->
<!-- METADATA - end -->
