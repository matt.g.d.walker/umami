## Incident Post-Mortem

### Checklist

 - [ ] Could the INC have been handled better?
 - [ ] Are the alerts defined appropriately?
 - [ ] Is there an infrastructure change or a code change that would prevent the INC reoccurence?
 - [ ] Is there an infrastructure change or a code change that would enhance the ability to handle a similar INC?

### What is the INC that was handled?



### What is the timeline of the INC and its handling?



### What is the root cause analysis of the INC?



### What was the actual adverse impact of the INC?



### Are there follow up recommendations?

