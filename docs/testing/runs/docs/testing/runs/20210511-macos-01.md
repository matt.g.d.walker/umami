# Umami Wallet - Abridged Test Run

**Date**: 2021-05-11

## Test Parameters

### OS

| Setting | Value |
|--|--|
| Operating System | Mac |
| Flavor (if applicable) | N/A |

### Components

| Component | Tag or Branch |
|--|--|
| umami | 0.3.6 |
| tezos-indexer | v9.1.8 |
| mezos | v1.0.1 |
| tezos | v9.1


### Settings

| Setting | Value |
|--|--|
| Node | https://florence-tezos.giganode.io |
| Mezos | https://api.umamiwallet.com/florencenet |


## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.


### Accounts and Operations
- [X] Send normal transaction 
- [X] Observe block count and confirmation 
- [ ] Observe explorer, use explorer link 
- [X] Send transaction to or from a derived Account
- [X] Send a Batch transaction
- [X] Select Advanced Options


### Delegations
- [X] Delegate an Account 
- [X] Observe the delegation 
- [X] Change baker 
- [X] Delete delegation

### Tokens
- [X] Register token FA1.2 contract 
- [X] Delete token contract 
- [X] Send token 
- [X] Send token batch 



### Issues to Verify
N/A



## Issues Found

* #240
