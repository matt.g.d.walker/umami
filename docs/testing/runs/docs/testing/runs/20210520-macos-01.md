# Umami Wallet - Test Run

**Date**: 2021-05-20

## Test Parameters

### Components

| Component | Tag or Branch |
|--|--|
| umami | v.0.3.8 |
| tezos-indexer | v9.1.8 |
| mezos | v2.0.0 |
| tezos | v9.1


### Settings

| Setting | Value |
|--|--|
| Node | https://florencenet.smartpy.io/ |
| Mezos | https://api.umamiwallet.com/florencenet |


## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.

### Onboarding
- [X] Create new account
- [X] Import account

### Accounts and Operations
- [X] Send normal transaction 
- [X] Observe block count and confirmation 
- [X] Observe explorer, use explorer link 
- [X] Send transaction to or from a derived Account
- [X] Send a Batch transaction mixing tez and tokens
- [X] Import a csv file and send a Batch transaction
- [X] Edit a transaction within a Batch
- [X] Delete a transaction within a Batch
- [X] Select Advanced Options
- [X] Import account
- [X] Export a secret
- [X] Rename a secret
- [X] Delete a secret
- [X] Create derived Account
- [X] Rename derived Account
- [X] Add contact from Operations

### Address Book
- [X] Create alias
- [X] Observe copy&paste 
- [X] Observe QR code
- [X] Rename alias
- [X] Observe alias in Send form
- [X] Delete alias


### Delegations
- [#248] Delegate an Account 
- [#248] Observe the delegation 
- [#248] Change baker 
- [X] Delete delegation

### Tokens
- [X] Register token FA1.2 contract 
- [X] Delete token contract 
- [X] Send token 
- [X] Send token batch 

### Settings
- [X] Change verification blocks setting 
- [X] Observe new blocks setting in Operations
- [X] Change dark/light mode
- [X] Reset settings
- [X] Network switch
- [X] Offboard wallet

### Issues to Verify

- [ ] #184 Add delete button to textfields dedicated to KT/TZ addresses
- [ ] #207 Wrong password on scanning deviated accounts silently fails
- [X] #138 Forms: inline password error in form instead of using logs
- [X] #234 Visual bugs in batch transaction list
- [X] #217 Fix batch view
- [X] #194 Allow batch transactions that mix token and tea transfers
- [X] #230 Batch from CSV file
- [X] #150 Fees: SDK/Taquito


## Issues Found

* #247 Editing batch transaction>user input "lost"
* #248 Cannot delegate account
