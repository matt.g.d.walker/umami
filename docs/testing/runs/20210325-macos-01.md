# Umami Wallet - Test Run

**Date**: 2021-03-25

## Test Parameters

### Components

| Component | Tag or Branch |
|--|--|
| umami | develop |
| tezos-indexer | --- |
| mezos | --- |
| tezos | --- |

### Settings

| Setting | Value |
|--|--|
| Node | https://testnet-tezos.giganode.io/ |
| Mezos | https://qa-api.umamiwallet.com/edo2net |

## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.

### Onboarding
- [X] Create new account
- [X] Import account

### Accounts and Operations
- [X] Send normal transaction
- [X] Observe block count and confirmation
- [ ] Observe explorer, use explorer link
- [X] Create derived Account
- [X] Rename derived Account
- [X] Send transaction to or from a derived Account
- [X] Send a Batch transaction
- [X] Edit a transaction within a Batch
- [X] Delete a transaction within a Batch
- [X] Export a secret
- [X] Rename a secret
- [X] Delete a secret
- [ ] Select Advanced Options

### Address Book
- [X] Create alias
- [X] Observe c&p and QR code
- [X] Rename alias
- [X] Delete alias
- [X] Observe alias in Send form

### Delegations
- [X] Delegate an Account
- [X] Observe the delegation
- [X] Change baker
- [X] Delete delegation

### Tokens
- [X] Register token FA1.2 contract
- [X] Delete token contract
- [X] Send token
- [X] Send token batch

### Settings
- [X] Change verification blocks setting and Observe
- [X] Change dark/light mode
- [X] Reset settings
- [X] Offboard wallet

## Issues
* #131
* #132
* #133
* #134
* #135
* #136
* #137
