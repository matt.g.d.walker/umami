# Umami Wallet - Test Checklist

**Date**: 2021-04-09

## Test Parameters

### OS

| Setting | Value |
|--|--|
| Operating System | Linux |
| Flavor (if applicable) | Fedora 33 |

### Components

| Component | Tag or Branch |
|--|--|
| umami | 20210408-rc1 |
| tezos-indexer | v9.1.5 |
| mezos | tag20210401 |
| tezos | v9.0-rc1 |
| taquito | 8.0.6-beta.0 |

### Settings

| Setting | Value |
|--|--|
| Node | https://edonet.smartpy.io |
| Mezos | https://api.umamiwallet.com/edo2net |

## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.

### Onboarding
- [x] Create new account
- [x] Import account

### Accounts and Operations
- [x] Send normal transaction
- [x] Observe block count and confirmation
- [ ] Observe explorer, use explorer link
- [x] Send transaction to or from a derived Account
- [x] Send a Batch transaction
- [x] Edit a transaction within a Batch
- [x] Delete a transaction within a Batch
- [x] Select Advanced Options
- [x] Import account
- [x] Export a secret
- [x] Rename a secret
- [x] Delete a secret
- [x] Create derived Account
- [x] Rename derived Account
- [x] Select Advanced Options
- [x] Add contact from Operations

### Address Book
- [x] Create alias
- [x] Observe c&p
- [x] Observe QR code
- [x] Rename alias
- [x] Observe alias in Send form
- [x] Delete alias


### Delegations
- [x] Delegate an Account
- [x] Observe the delegation
- [x] Change baker
- [x] Delete delegation

### Tokens
- [x] Register token FA1.2 contract
- [x] Delete token contract
- [x] Send token
- [x] Send token batch

### Settings
- [x] Change verification blocks setting
- [x] Observe new blocks setting in Operations
- [x] Change dark/light mode
- [x] Reset settings
- [x] Offboard wallet

### Issues to Verify

 * [x] Double import of secret key [#129]
 * [ ] Taquito Fees [#150]

## Issues Found

 * [#160] Import screen is misleading if I already have a secret: it asks for a new password for this key, but actually requires the current password.
 * [#161] Adding a contact with the same key as another one actually renames the previous contact.
   * After this specific step, if I add another alias with the same name but another key, it has its name and its key, but the one previously overwritten reappears.
 * [#162] Adding a contact with an invalid key results in an empty error in the logs.
 * [#163] Changing for an unregistered delegate results in an [Object] error in the logs. The console actually gives an the "unregistered" error.
 * [#164] Custom fees are not shown in the recap, but correctly set.
 * [#78] If I set custom fees and go edit my transaction in a batch afterwards, the fees for this transaction are recalculated and set to the new one.
 * [#165] Token batch advanced options: the second and + fees are wrongly computed, and the one shown is actually the sum of all the fees.
 * [#117] One transaction seems stuck on 6/6 blocks, while the other above are confirmed.
 * [#150] Any kind of transaction: default fees simulated are not the one sent by Taquito.
