# Umami Wallet - Test Run

**Date**: 2021-05-12

## Test Parameters

### Components

| Component | Tag or Branch |
|--|--|
| umami | v0.3.9 |
| tezos-indexer | v9.1.9 |
| mezos | v2.0.0 |
| tezos | v9.2


### Settings

| Setting | Value |
|--|--|
| Node | https://florence-tezos.giganode.io |
| Mezos | https://api.umamiwallet.com/florencenet |


## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.

### Onboarding
- [x] Create new account
- [x] Import account

### Accounts and Operations
- [x] Send normal transaction
- [x] Observe block count and confirmation
- [x] Observe explorer, use explorer link
- [x] Send transaction to or from a derived Account
- [x] Send a Batch transaction
- [x] Edit a transaction within a Batch
- [x] Delete a transaction within a Batch
- [x] Select Advanced Options
- [x] Import account
- [x] Export a secret
- [x] Rename a secret
- [x] Delete a secret
- [x] Create derived Account
- [x] Rename derived Account
- [x] Add contact from Operations

### Address Book
- [x] Create alias
- [x] Observe copy&paste
- [x] Observe QR code
- [x] Rename alias
- [x] Observe alias in Send form
- [x] Delete alias


### Delegations
- [x] Delegate an Account
- [x] Observe the delegation
- [x] Change baker
- [x] Delete delegation

### Tokens
- [x] Register token FA1.2 contract
- [x] Delete token contract
- [x] Send token
- [x] Send token batch

### Settings
- [x] Change verification blocks setting
- [x] Observe new blocks setting in Operations
- [x] Change dark/light mode
- [x] Reset settings
- [x] Network switch
- [x] Offboard wallet

### Issues to Verify

## Issues Found
