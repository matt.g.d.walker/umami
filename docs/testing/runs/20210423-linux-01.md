# Umami Wallet - Test Checklist

**Date**: 2021-04-23

## Test Parameters

### OS

| Setting | Value |
|--|--|
| Operating System | Linux |
| Flavor (if applicable) | Fedora 33 |

### Components

| Component | Tag or Branch |
|--|--|
| umami | 0.3.5 |
| tezos-indexer | 9.1.2 |
| mezos | 1.0.0 |
| tezos | 9.0~rc1 |

### Settings

| Setting | Value |
|--|--|
| Testnet |
|--|--|
| Node | https://edonet.smartpy.io/ |
| Mezos | https://api.umamiwallet.com/edo2net |
|--|--|
| Mainnet |
|--|--|
| Node | https://mainnet.smartpy.io/ |
| Mezos | https://api.umamiwallet.com/mainnet |

## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.

### Onboarding
- [x] Create new account
- [x] Import account

### Accounts and Operations
- [x] Send normal transaction
- [x] Observe block count and confirmation
- [x] Observe explorer, use explorer link
- [x] Send transaction to or from a derived Account
- [x] Send a Batch transaction
- [x] Edit a transaction within a Batch
- [x] Delete a transaction within a Batch
- [x] Select Advanced Options
- [x] Import account
- [x] Export a secret
- [x] Rename a secret
- [x] Delete a secret
- [x] Create derived Account
- [x] Rename derived Account
- [x] Add contact from Operations

### Address Book
- [x] Create alias
- [x] Observe c&p
- [x] Observe QR code
- [x] Rename alias
- [x] Observe alias in Send form
- [x] Delete alias


### Delegations
- [x] Delegate an Account
- [x] Observe the delegation
- [x] Change baker
- [x] Delete delegation

### Tokens
- [x] Register token FA1.2 contract
- [x] Delete token contract
- [x] Send token
- [x] Send token batch

### Settings
- [x] Change verification blocks setting
- [x] Observe new blocks setting in Operations
- [x] Change dark/light mode
- [x] Reset settings
- [ ] Offboard wallet

### Issues to Verify

 * [ ] #189: I successfully added a new account to a master key with path `m/43'/1729'/0'/0'`
 * [x] #213: Int overflow on tokens
 * [x] #223
 * [x] #188
 * [x] #192
 * [x] #204
 * [x] #189
 * [ ] #199: no scroll like for other windows
 * [x] #201
 * [x] #198
 * [ ] #190: still happening for some sizes
 * [x] #218
 * [x] #195

## Issues Found

 * No derivation path on Import account.
 * `Operation submitted` modal oversized.
