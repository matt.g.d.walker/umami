# Umami Wallet - Test Run

**Date**: 2021-04-12

## Test Parameters

### OS

| Setting | Value |
|--|--|
| Operating System | Mac |
| Flavor (if applicable) | N/A |

### Components

| Component | Tag or Branch |
|--|--|
| umami | [TAG] |
| tezos-indexer | [TAG] |
| mezos | [TAG] |
| tezos | [TAG] |

### Settings

| Setting | Value |
|--|--|
| Node | https://edonet.smartpy.io/ |
| Mezos | https://api.umamiwallet.com/edo2net |

## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.

### Onboarding
- [X] Create new account
- [X] Import account

### Accounts and Operations
- [X] Send normal transaction
- [X] Observe block count and confirmation
- [X] Observe explorer, use explorer link
- [X] Send transaction to or from a derived Account
- [X] Send a Batch transaction
- [X] Edit a transaction within a Batch
- [X] Delete a transaction within a Batch
- [X] Select Advanced Options
- [X] Import account
- [X] Export a secret
- [X] Rename a secret
- [X] Delete a secret
- [X] Create derived Account
- [X] Rename derived Account
- [X] Add contact from Operations

### Address Book
- [X] Create alias
- [X] Observe c&p
- [X] Observe QR code
- [X] Rename alias
- [X] Observe alias in Send form
- [X] Delete alias


### Delegations
- [X] Delegate an Account
- [X] Observe the delegation
- [X] Change baker
- [X] Delete delegation

### Tokens
- [X] Register token FA1.2 contract
- [X] Delete token contract
- [X] Send token
- [X] Send token batch

### Settings
- [X] Change verification blocks setting 
- [X] Observe new blocks setting in Operations
- [X] Change dark/light mode
- [X] Reset settings
- [X] Offboard wallet

### Issues to Verify

 *

## Issues Found

 * #172
