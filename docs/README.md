# Reference Wallet - Documentation Folder

* `/artifacts/`: contains "dated" documentation that served a purpose at a point in time. The documents therein are not meant to be kept up-to-date; they are rather a reflection of its time.
* `/operative/`: contains documentation that is intended to reflect the current state of the product or project.
* `/specs/`: contains documentation of a technical nature; usu. interface specifications.
* `/standards/`: contains documentation that prescribes best-practices for project efforts.

If you're looking to get acquainted with this project and its details, see the [Charter](/docs/operative/charter.md)