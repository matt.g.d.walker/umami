# Ref-Wallet - Backlog

Here is the list of features that have not yet been implemented:

1. FA2 (includes NFT)
1. Remote Signing (TZIP-10)
1. Direct Auth
1. Ledger support
1. Mobile
1. Contracts: originate and interact
1. automatic backup of wallet files to Google drive / Dropbox / iCloud (opt in)
1. Sapling
1. Support payment links (TZIP-8)
1. Support payto links: [RFC8905](https://tools.ietf.org/html/rfc8905)
1. Multisig
1. Spending limits
1. Decentralized ID
1. Cryptonomic image mirror
