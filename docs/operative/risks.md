# Ref-Wallet - Risk Matrix

| ID | Status | Title | Description | Worst Case | Probability | Impact | Treatment | Follow-up | Resolution |
|--|--|--|--|--|--|--|--|--|--|
| RSK0001 | Closed | ReasonML Resource Availability | ReasonML developers may be sparse | Insufficient resources--slow development | Med | High | Mitigation: targetted search for candidates | Review if project bogs down | Not so difficult to find after all |
| RSK0002 | Closed | UX Experience | NL does not have UX experience in-house | Insufficient resources--slow development | -- | High | Mitigation: Hire specialist | Review if performance is lacking | Found appropriate experienced member |
| RSK0003 | Mitigated | Project Difficulty | Large project with many features | Failure to complete all or parts of system | -- | High | Mitigation: formal Proj Mngt; build competent team | Review if project bogs down | |
| RSK0004 | Mitigated | New Language | ReasonML is not familiar to many; but similar to OCaml | Slow development; low morale | -- | High | Mitigation: ReasonML training; hire experienced member(s) | Review if project bogs down | |
| RSK0005 | Mitigated | Code Quality | ReasonML is not familiar by starting team; quality might suffer | Low quality--high maintenance cost | -- | Med | Mitigation: ReasonML training; hire experienced member(s); review codebase | Review when experienced members join | |
| RSK0006 | Mitigated, for Review | No Client Lib | Tezos Client is a CLI tool; no good alternatives internally | CLI calls perform badly and present man-in-the-middle vulnerabilities | High | High | Additional resources drawn to compile Tezos Client to JS | | |
| RSK0007 | Identified | Application Security | The app must meet security standards | Vulnerable app, comprises | -- | Severe | Security audit at various milestones prior to launch; discuss with Security WG | Review candidate releases | |
| RSK0008 | Identified | Appropriate Attention/Outreach | The app should get a sufficient amount of user adoption and attain a good reputation/image | Ref-Wallet remains obscure, unknown, ignored | -- | High | Manager to coordinate with TCF & Comm Dept to establish strategy; incl: website, manuals, announcements | | |
| RSK0009 | Identified | Support Structure | There is insufficient support structure in NL for a new product | Frustrated users, disconnected from community | High | Med | Manager to propose a support structure | | |
