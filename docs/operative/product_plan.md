# Ref-Wallet - Product Roadmap

> As of Oct 2020

The **Product** roadmap plans the To-Market efforts to capture public attention and drive adoption.

## Projected Milestones:

 * Alpha version: Nov 2020 (internal only--usable with basic feature set)
 * Beta release: Apr 2021 (limited release--more features, some left in the backlog: TBD)
 * v1.0 release: Jun 2021 (public release--stable)
 * v1.x maintenance phase
 * v2.0 release: Q4 2021 (public release--no backlog)

## Strategy

 * Branding
     * Name
     * Logo/Icon
     * Theme
     * Trademark
 * Website
     * Domain
     * Graphic design
 * Social Media
     * Tweets
     * Blog
     * Community Engagement
 * Technical Support
     * L1 - Runbooks
     * Escalation Rules
     * L2 & L3 - Support Roster and Schedule
 * Documentation
     * User Manual
     * Help
     * Collaboration with Wallet Working Group

## Gantt Chart

```mermaid
gantt
    title Ref-Wallet Product Roadmap
    section Alpha
    Approve Branding                                  :done  , 2020-09-25, 2021-01-13
    Planning                                          :done  , 2020-11-13, 2020-11-20
    Simple website                                    :done  , 2020-12-01, 2020-12-30
    Social Media Profiles                             :done  , 2020-11-20, 2020-11-30
    Announce Project                                  :done  , 2021-01-15, 2021-01-31
    section Beta
    Upgrade website                                   :done  , 2021-01-15, 2021-01-31
    section Release - v 1.0
    Setup Infrastructure                              :done  , 2021-02-01, 2021-03-15
    Draft User Manual Articles                        :active, 2021-03-01, 2021-05-01
    Establish Support Structure                       :active, 2021-03-15, 2021-05-31
    Draft Runbooks & Support Rules                    :active, 2021-04-01, 2021-05-31
    Integrate Help Documentation                      :active, 2021-04-15, 2021-05-31
    section Maintenance - v 1.x
    Maintenance - v 1.x                               :active, 2021-04-01, 2021-06-01
    section Upgrade - v 2.0
    Upgrade - v 2.0                                   :active, 2021-06-01, 2021-09-01
```
