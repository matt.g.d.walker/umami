Ref Wallet - Standard Operating Procedures

1. Show up to meetings on time, or notify of absence ahead of time
1. You are responsible for your estimates
    1. Estimates can be amended, but must be done so as-soon-as estimate is foreseeably unlikely to be achieved
1. Build and design with testing in mind
1. All new functionality gets a Unit Test
1. All newly discovered/fixed bugs get a Unit Test (or expand an existing one for coverage)
1. Log all issues encoutered with project dependency systems, as GitLab issues and communicate them to NL Manmagement (Bruno & Mathias)
