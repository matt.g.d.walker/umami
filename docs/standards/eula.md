# Umami Wallet End User License Agreement

## Agreement
By making use of Umami, User agrees to follow terms as set herein.

## Umami Description
The Umami Wallet (hereafter "Umami") is an open source software project supported by Nomadic Labs, a company incorporated in France. Umami is a simple account management system for Tezos' cryptocurrency (hereafter "tez").

Umami's intended purpose is to: (i) allow users to view their tez balances; (ii) facilitates the forming operations for submission to the blockchain; and (iii) allows users to save and organize their contacts’ addresses.

# Umami is not a VASP
Umami is not a Virtual Asset Service Provider, as it does not provide the following services: (i) recommendations or advice; or (ii) trade or swap between tokens or assets.

Umami is free of charge. Umami does not profit from its operations, nor does Umami collect any user's personal data centrally.

# No Guarantees
As Umami is an open source project, the Umami Team members cannot guarantee that any errors of any nature that could possibly appear in the source code “have been seen.” The Umami Team members, Nomadic Labs, its subsidiaries, the directors, employees and agents cannot under any circumstances be held liable for the use of and reliance by the Users or any third Party whatsoever on Umami.  

Umami's source code reflects the best of our knowledge, it has been made in good faith and while every care has been taken in coding Umami. The Umami Team members, Nomadic Labs, its subsidiaries, the directors, employees and agents make no engagement and give no warranties of whatever nature with respect to the Umami source code, including but not limited to any bugs, faults or risk of loss of your crypto-assets. 

No content on Umami shall constitute or shall be understood as a recommendation to enter any investment transactions or agreements of any kind, nor to use the code without having carried out your own technical audit. 

# No Advice and Appropriateness
Before taking any action in using this “Software”, User should consider Umami relevance to their particular circumstances, and User is strongly advised to consult a professional technical advisor, verified and reputable agent from a professional third party. The Umami Team members, Nomadic Labs, its subsidiaries, the directors, employees and agents do not make any warranties about the completeness, reliability and accuracy or fitness for lawful use of the software of the information presented herein.

# Not a Subcontractor Contract
Use of Umami and agreement of the terms set herein cannot be considered a subcontractor contract.

# Hold Harmless
User understands and agrees that any action on Umami is strictly at User's own risk. The Umami Team members, Nomadic Labs, its subsidiaries, the directors, employees and agents will not be held liable for any losses and damages in connection to the use of the Umami source code.

# Lawful Use
User shall comply with all applicable laws, statutes, regulations and codes relating to anti-bribery and anti-corruption, fight against the financing of terrorism and shall not engage in any activity, practice or conduct outside France, which would constitute an offence of the previous texts, if such activity, practice or conduct had been carried out inside France. 
None of the Umami Team members, Nomadic Labs, its subsidiaries, the directors, employees and agents shall be considered liable of such misconduct by a malicious User.
